<?php

namespace App\Http\Controllers;


use App\DTO\SwapiResponse;
use App\Starship;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use JMS\Serializer\SerializerBuilder;

class HomeController extends Controller
{
    protected $guzzle;
    protected $serializer;

    public function __construct(Client $guzzle)
    {
        $this->guzzle = $guzzle;
        $this->serializer = SerializerBuilder::create()->build();
    }

    public function getStarships(Request $request)
    {
        return view('welcome', ['totalStarships' => Starship::all()->count()]);
    }
}