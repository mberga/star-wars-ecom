<?php

namespace App\Services;


use App\Starship;

class StarshipSyncronizer
{
    public static function saveResults(array $result)
    {
        foreach ($result['results'] as $starshipArray) {
            $starship = Starship::find($starshipArray['url']) ?: new Starship();
            foreach ($starshipArray as $key => $value) {
                if (is_array($value)) $value = json_encode($value);
                if ($key == 'created' || $key == "edited") continue;
                $starship->setAttribute($key, $value);
            }

            print_r($starship->getAttributes());
            $starship->save();
        }
    }
}