# star-wars-ecom

Technical test about star wars ecommerce with swapi.com

To run the app, just do `docker-compose up` and navigate to http://localhost

To connect to mysql, use `localhost:3306` with user/passw: `root`

If it's the first time running this app, you may need to create all database structure, it's as easy as
`docker exec -ti star-wars-ecom_web-server_1 bash -c "cd /var/www/html && php artisan migrate"`

When ready, use `npm run dev` or `npm run prod` to compile components and get ready.

Vue components can be found at `resources/js/components` 

### How does it work?
`/` (home) path shows a by 10 elements paginated list, loaded via AJAX. Each page makes a query to load next page.
Filtering also makes AJAX request to get filtered elements.
AJAX route can be found at `/api/starships`