<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStarshipTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('starships', function (Blueprint $table) {
            $table->string("url")->primary();
            $table->string('name');
            $table->string('model');
            $table->string('manufacturer');
            $table->integer('cost_in_credits');
            $table->integer("length");
            $table->string('max_atmosphering_speed');
            $table->integer('crew');
            $table->integer('passengers');
            $table->integer('cargo_capacity');
            $table->string('consumables');
            $table->float('hyperdrive_rating');
            $table->integer('mglt');
            $table->integer('starship_class');
            $table->json('pilots');
            $table->json('films');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('starship');
    }
}
