<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!-- Compiled and minified CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    </head>
    <body>
    <nav>
        <div class="nav-wrapper">
            <a href="#" class="brand-logo">
                <div class="valign-wrapper">
                    <img src="{{asset('img/logo.png')}}" height="60" /> <span class="margin-left-10">Star wars 'n loud</span>
                </div>
            </a>
        </div>
    </nav>
        <div id="app">
            <div class="row">
                <div class="col m3">
                    <filter-component></filter-component>
                </div>
                <div class="col m9">
                    <div class="progress" id="starship-progress">
                        <div class="indeterminate"></div>
                    </div>
                    <starship-list></starship-list>

                </div>
            </div>
            <modal-starship></modal-starship>
        </div>


        <footer class="page-footer">
            <div class="container">
                <div class="row">
                    <div class="col l6 s12">
                        <h5 class="white-text">Star wars'n Loud</h5>
                        <p class="grey-text text-lighten-4">The best stolen (or not) starship in the galaxy!.</p>
                    </div>
                    <div class="col l4 offset-l2 s12">
                        <h5 class="white-text">Links</h5>
                        <ul>
                            <li><a class="grey-text text-lighten-3" href="#!">Privacy policy (yes, we don't care so mouch)</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-copyright">
                <div class="container">
                    © 2019 Mos Eisley Cantine
                    <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
                </div>
            </div>
        </footer>

        <script>
            const myAppConfig = {
                paginator_page_count: {{$totalStarships / 10}}
            }
        </script>

        <!-- Compiled and minified JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
        <script src="{{asset("js/app.js")}}"></script>
    </body>
</html>