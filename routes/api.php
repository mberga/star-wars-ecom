<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/starships', function (Request $request) {
    $where = [];

    $request->get('str') ? $where["name"] = $request->get('str') : null;

    $query = \App\Starship::query();

    if ($request->get('str')) {
        $query = $query->where('name', 'LIKE', "%" . $request->get('str') . "%")
            ->orWhere('model', 'LIKE', "%" . $request->get('str') . "%");
    }

    $starships = $query->paginate(10, ['*'], 'page', $request->get('page') ?: 1);
    $array = [];
    foreach ($starships as $starship) $array[] = $starship->jsonSerialize();
    return new \Symfony\Component\HttpFoundation\JsonResponse($array);
});
