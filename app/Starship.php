<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Starship extends Model
{
    protected $guarderd = [];

    protected $fillable = ['*'];

    protected $primaryKey = "url";

    public $incrementing = false;

    protected $keyType = "string";
}
