<?php

use Illuminate\Foundation\Inspiring;
use App\DTO\SwapiResponse;
use JMS\Serializer\SerializerBuilder;
use GuzzleHttp\Client;
/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');


Artisan::command('update-starships', function() {
    $guzzle = new Client();
    $url = 'https://swapi.co/api/starships';
    do {
        $request = $guzzle->get($url);
        $result = json_decode($request->getBody()->getContents(), true);
        \App\Services\StarshipSyncronizer::saveResults($result);
        $url = $result['next'];
    } while ($result['next']);
})->describe('update starship products');