<?php

namespace App\DTO;


use App\Starship;
use JMS\Serializer\Annotation as Serializer;

class SwapiResponse
{
    /**
     * @var int
     * @Serializer\Type("int")
     */
    protected $count;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    protected $next;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    protected $previous;

    /**
     * @var Starship[]
     * @Serializer\Type("array<App\Starship>")
     */
    protected $results;

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @return string
     */
    public function getNext(): string
    {
        return $this->next;
    }

    /**
     * @return string
     */
    public function getPrevious(): string
    {
        return $this->previous;
    }

    /**
     * @return Starship[]
     */
    public function getResults(): array
    {
        return $this->results;
    }

    /**
     * @param int $count
     * @return SwapiResponse
     */
    public function setCount(int $count): SwapiResponse
    {
        $this->count = $count;
        return $this;
    }

    /**
     * @param string $next
     * @return SwapiResponse
     */
    public function setNext(string $next): SwapiResponse
    {
        $this->next = $next;
        return $this;
    }

    /**
     * @param string $previous
     * @return SwapiResponse
     */
    public function setPrevious(string $previous): SwapiResponse
    {
        $this->previous = $previous;
        return $this;
    }

    /**
     * @param Starship[] $results
     * @return SwapiResponse
     */
    public function setResults(array $results): SwapiResponse
    {
        $this->results = $results;
        return $this;
    }
}